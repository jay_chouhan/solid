# SOLID
--------

## Introduction

[**SOLID**][1][^1] is a set of five [software design principles][2][^2]\, which are implemented in [Object Oriented Design][3][^3] to make a code:
* understandable
* flexible
* [maintainable][4][^4]
as much as possible.


These principles are a subset of design principles promoted by the famous software engineer and instructor  [*Uncle BOB*][5][^5]\. The goal of SOLID is to write a superior and clean code that is easy to understand, can be easily migrated and implemented throughout different interfaces.

## How to implement SOLID?

![Solid Principles][6][^6]

The term **SOLID** is a mnemonic acronym for 5 principles which are as follows:

* **S** - Single-Responsibility Principle
* **O** - Open–Closed Principle
* **L** - Liskov Substitution Principle
* **I** - Interface segregation Principle
* **D** - Dependency Inversion Principle

let's look at how these concepts are implemented and help us in designing superior code.

#### Single-Responsibility Principle

> ###### *A class should have one and only one reason to change, meaning that a class should have only one job.*

This means that a class should only be given one task so that it would only depend on one actor. This helps in maintaining the software as due to this, we don't have to change code as often when something changes.
It also makes understanding the code easier because unrelated concepts in the same class also make comprehending the purpose of the code harder.

Let's take the below code for example:

```javascript
    class UserSettings {
        constructor(user) {
        this.user = user;
        }

        changeSettings(settings) {
            if (this.verifyCredentials()) {
                 // ...
            }
        }

        verifyCredentials() {
             // ...
        }
    }
```

As we can see in the above code the *class* **UserSettings** have two functions **changeSettings(settings)** and **verifyCredentials()**. So, we can see the class is performing two different actions.

So instead of the above code we should use the below code:

``` javascript
    class UserAuth {
        constructor(user) {
        this.user = user;
        }

        verifyCredentials() {
        // ...
        }
    }


    class UserSettings {
        constructor(user) {
            this.user = user;
            this.auth = new UserAuth(user);
        }

        changeSettings(settings) {
            if (this.auth.verifyCredentials()) {
                // ...
            }
        }
    }
```

As we can see now we have two classes for two different functions. This follows the principle of **Single-Responsibility Principle**.

#### Open–Closed Principle

> ###### *Software entities should be open for extension, but closed for modification.*

Software Systems must be allowed to add new code rather than changing the existing code. let's see an example:

``` javascript
    const iceCreamFlavours = ["Chocolate", "Vanilla"]
    checkFlavour = (iceCream) => {
        if(iceCreamFlavours.includes(iceCream.flavour)){
            return true; 
        }else{
            return false
        }
    }

    checkFlavour("Vanilla"); //true
    checkFlavour("Strawberry"); //false
```

Now if in the above code if we want to add another flavour, say "Strawberry", instead of adding it manually to the **iceCreamFlavours** array we should create another function *addFlavour()* to add a new flavour to the array, hence the code above will not be modified but will be extended. See the below code for a better understanding.

``` javascript
    //UNTOUCHABLE CODE!!!
    const iceCreamFlavours = ["Chocolate", "Vanilla"]
    checkFlavour = (iceCream) => {
        if(iceCreamFlavours.includes(iceCream.flavour)){
            return true; 
        }else{
            return false
        }
    }

    //UNTOUCHABLE CODE!!!

    //We can define a function to add a new role with this function
    addFlavour(flavour){
        iceCreamFlavours.push(flavour)
    }

    //Call the function with the new role to add to the existing ones
   
    addFlavour("Strawberry");


    checkFlavour("Chocolate"); //true
    checkFlavour("Strawberry"); //true
```

So in this way, we could extend the code without actually modifying it and follow the **Open–Closed Principle**.

#### Liskov Substitution Principle

> ###### *Functions that use pointers or references to base classes must be able to use objects of derived classes without knowing it.*

What this means is that if *S* is a Subtype of *T*, then objects of type *T* may be replaced with objects of type *S*. In other words the base class must be completely substitutable for their base types.
Let's see the below example:

``` javascript
    class Dogs{
        name(){
            console.log("Tommy")
        }
        legs(){
            console.log("Four")
        }
    }
    class Monkeys extends dogs{
        name(){
            console.log("Apu")
        }
    }
```

In the above code, we can see that the *class* **Monkey** extends the *class* **Dogs**. The dogs *class* has **legs()** *function* which prints Four but we know that monkeys don't have four legs, hence the **Dogs** class isn't completely substitutable with **Monkeys** class. We can correctly apply this code in the following way.

``` javascript
    class animals{
        name(){
            //
        }
        legs(){
            //
        }
    }
    
    class Dogs extends Animals{
        name(){
            console.log("Tommy")
        }
        legs(){
            console.log("Four")
        }
    }
    class Monkeys extends Animals{
        name(){
            console.log("Apu")
        }
        legs(){
            console.log("Two")
        }
    }
```

In this way, we can correctly apply the **Liskov Substitution Principle**.

#### Interface Segregation Principle

> ###### *No client should be forced to depend on methods it does not use.*

What this means is, instead of using a single interface (class in javascript) for various modules and functions, we should use many small interfaces and not load an interface with functions it may not even use. So we should not bulk up a single interface or class with multiple functions and methods.

Let's see an example:

``` javascript
    class User {
        constructor(username, password) {
            this.username = username;
            this.password = password;
            this.initiateUser();
        }
        initiateUser() {
        this.username = this.username;
        this.validateUser()
        }

        validateUser = (user, pass) => {
        console.log("validating...");
        }
    }
    const user = new User("Francesco", "123456");
    console.log(user);
```

As we can see in the above code, the *class* **User** contains both **initiateUser** and **validateUser** functions hence it will do both validating and then initiating the user. But what if validating the user is not required? Therefore, what we need to do is validate the user only if necessary. See the code below:

``` javascript
    class User {
        constructor(username, password, validate) {
            this.username = username;
            this.password = password;
            this.validate = validate;
            if (validate) {
                this.initiateUser(username, password);
            } else {
                console.log("no validation required");
            }
        }

        initiateUser() {
            this.validateUser(this.username, this.password);
        }

        validateUser = (username, password) => {
            console.log("validating...");
        }
    }

    //User with validation required
    console.log(new UserISP("Francesco", "123456", true))

    //User with no validation required
    console.log(new UserISP("guest", "guest", false));
```

We can see the above code only validates the user when it's necessary. In this way, we should follow the **Interface Segregation Principle**.

#### Dependency Inversion Principle

> ###### *High-level modules should not depend on low-level modules. Both should depend on abstractions.*

> ###### *Abstractions should not depend on details. Details should depend on abstractions.*

What this means is that we shouldn't have to know the implementation details of our dependencies. If we do, then we have violated this principle. 

We need this because if we need to reference our code for implementation of dependencies then if the dependency changes later our code will break a lot. This will become a very complex problem as we go on writing big blocks of code.

Let's see an example:

``` javascript
    http.get("http://address/api/examples", (res) => {
        this.setState({
            key1: res.value1,
            key2: res.value2,
            key3: res.value3
        });
    });
```
Here we can see that the HTTP request function depends on the detail setState. We can solve this principle by setting the state in another function.

``` javascript
    //Http request
    const httpRequest = (url, setState) => {
        http.get(url, (res) => setState.setValues(res))
    };

    //State set in another function
    const setState = {
        setValues: (res) => {
            this.setState({
                key1: res.value1,
                key2: res.value2,
                key3: res.value3
            })
        }
    }

    //Http request, state set in a different function
    httpRequest("http://address/api/examples", setState);

```

In this way, we can apply the **Dependency Inversion Principle**.

## Summary

What we have learnt from above is that implementing the SOLID principles will help us make our code easy to adjust, extend and test with little to no problems. We must apply these rules to easily manage and maintain our code.

## References

#### Videos and Codes

1. https://www.youtube.com/watch?v=XzdhzyAukMM

2. https://codepen.io/beaucarnes/pen/gmowZd?editors=0010

3. https://dev.to/francescoxx/solid-principles-in-javascript-3pek

4. https://levelup.gitconnected.com/javascript-clean-code-solid-9d135f824180

#### Articles and Images

[^1]: https://en.wikipedia.org/wiki/SOLID
[^2]: https://www.geeksforgeeks.org/principles-of-software-design/
[^3]: https://en.wikipedia.org/wiki/Object-oriented_design
[^4]: https://en.wikipedia.org/wiki/Software_maintenance
[^5]: https://en.wikipedia.org/wiki/Robert_C._Martin
[^6]: https://miro.medium.com/max/5018/1*1Fl0dq4B7vq3zqR2k8bHdg.jpeg

[1]: https://en.wikipedia.org/wiki/SOLID
[2]: https://www.geeksforgeeks.org/principles-of-software-design/
[3]: https://en.wikipedia.org/wiki/Object-oriented_design
[4]: https://en.wikipedia.org/wiki/Software_maintenance
[5]: https://en.wikipedia.org/wiki/Robert_C._Martin
[6]: https://miro.medium.com/max/5018/1*1Fl0dq4B7vq3zqR2k8bHdg.jpeg
